package api

import (
	"devoplin/models"
	"devoplin/pkg/e"
	"devoplin/pkg/util"
	"github.com/astaxie/beego/validation"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type auth struct {
	Card     string `valid:"Required;"`
	Password string `valid:"Required;"`
}

func GetAuth(c *gin.Context) {
	card := c.PostForm("card")
	password := c.PostForm("password")

	valid := validation.Validation{}
	a := auth{Card: card, Password: password}
	ok, _ := valid.Valid(&a)

	data := make(map[string]interface{})
	code := e.INVALID_PARAMS
	if ok {
		isExist := models.CheckAuth(card, password)
		if isExist == 1 {
			token, err := util.GenerateToken(card, password)
			if err != nil {
				code = e.ERROR_AUTH_TOKEN
			} else {
				data["token"] = token

				code = e.SUCCESS
			}

		} else if isExist == 2 {
			code = e.ERROR_EXIST_EXCHANGE
		} else if isExist == 3 {
			code = e.ERROR_LOGIN
		} else {
			code = e.ERROR_AUTH
		}
	} else {
		for _, err := range valid.Errors {
			log.Println(err.Key, err.Message)
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  e.GetMsg(code),
		"data": data,
	})
}
