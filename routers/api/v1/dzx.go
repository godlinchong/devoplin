package v1

import (
	"devoplin/models"
	"devoplin/pkg/e"
	"devoplin/pkg/setting"
	"devoplin/pkg/util"
	"github.com/astaxie/beego/validation"
	"math/rand"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func PostDzx(c *gin.Context) {
	card := c.Query("card")
	name := c.PostForm("name")
	phoneNumber := c.PostForm("phone_number")
	orderTime := c.PostForm("order_time")
	address := c.PostForm("address")
	valid := validation.Validation{}

	valid.Required(card, "card").Message("卡号不存在,非法操作")
	valid.Required(address, "address").Message("地址不能为空")
	valid.Required(orderTime, "order_time").Message("预约时间不能为空")
	valid.MaxSize(phoneNumber, 11, "phone_number").Message("手机号码格式错误")
	valid.MaxSize(name, 100, "name").Message("名称最长为100字符")
	code := e.INVALID_PARAMS
	if !valid.HasErrors() {
		code = e.SUCCESS
		flag := models.ExistDzxByCard(card)
		if flag == 1 {
			data := make(map[string]interface{})
			data["name"] = name
			data["phone_number"] = phoneNumber
			data["order_time"] = orderTime
			data["address"] = address
			data["flag"] = true // 已兑换
			b := models.EditDzx(card, data)
			// 提交成功就发送成功提醒短信
			if b {
				sms := models.SMS{
					Card:        card,
					Mobiles:     setting.OthersSetting.Mobile,
					Name:        name,
					OrderTime:   orderTime,
					Address:     address,
					PhoneNumber: phoneNumber,
				}
				util.SendSms(GetApp(), sms)
			}
		} else if flag == 2 {
			code = e.ERROR_EXIST_EXCHANGE
		} else {
			code = e.ERROR_CARD_OR_PASSWD
		}
	}
	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg":  e.GetMsg(code),
		"data": make(map[string]string),
	})
}
func randKey() int {
	//step1: 设置种子数
	rand.Seed(time.Now().UnixNano())
	//step2：获取随机数
	return rand.Intn(len(setting.OthersSetting.App))
}
func GetApp() setting.Sdk {
	return setting.OthersSetting.App[randKey()]
}
