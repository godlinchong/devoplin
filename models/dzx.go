package models

type Dzx struct {
	Model
	Card        string
	Passwd      string `gorm:"size:255"`
	OrderTime   string `json:"order_time"`
	Name        string
	PhoneNumber string `json:"phone_number"`
	Address     string
	Flag        bool
	Order       string
	Mark        bool
	Remark      string
	Count       int
	CreatedBy   string `json:"created_by"`
	ModifiedBy  string `json:"modified_by"`
}

func AddDzx(card string, passwd string) bool {
	db.Create(&Dzx{
		Card:      card,
		Passwd:    passwd,
		CreatedBy: "林冲",
	})
	return true
}
func CheckAuth(card, password string) int {
	var dzx Dzx
	db.Select("id,flag,passwd,count").Where(Dzx{Card: card}).First(&dzx)
	if dzx.Count == 20 {
		return 3
	} else {
		if dzx.Passwd == password {
			// 卡号和对应的密码存在,然后在判断是否兑换过
			if dzx.Flag == true {
				// 兑换过了直接返回2
				return 2
			} else {
				return 1
			}
		} else {
			data := make(map[string]interface{})
			data["count"] = dzx.Count + 1
			db.Model(&Dzx{}).Where("card = ?", card).Updates(data)
		}
		return 0
	}

}

func ExistDzxByCard(card string) int {
	var dzx Dzx
	db.Select("id,flag").Where("card = ?", card).First(&dzx)
	if dzx.ID > 0 {
		// 卡号和对应的密码存在,然后在判断是否兑换过
		if dzx.Flag == true {
			// 兑换过了直接返回2
			return 2
		} else {
			return 1
		}
	}
	return 0
}

func EditDzx(card string, data interface{}) bool {
	db.Model(&Dzx{}).Where("card = ?", card).Updates(data)
	return true
}
