package models

type SMS struct {
	Card        string
	Mobiles     []string
	OrderTime   string
	Name        string
	PhoneNumber string
	Address     string
}
