package util

import (
	"devoplin/models"
	"devoplin/pkg/setting"
	"fmt"
	"github.com/zboyco/gosms"
)

// 创建Sender
//sender := &gosms.QSender{
////AppID:  "1400265529",                       // appid
////AppKey: "a2c6c16d0ae33224e39c81e634f28023", // appkey
////}
func SendSms(sdk setting.Sdk, sms models.SMS) {
	// 创建Sender
	sender := &gosms.QSender{
		AppID:  sdk.AppID,  // appid
		AppKey: sdk.AppKey, // appkey
	}
	for i := 0; i < len(sms.Mobiles); i++ {
		// 发送短信
		res, err := sender.SingleSend(
			"楚食官",          // 短信签名，此处应填写审核通过的签名内容，非签名 ID，如果使用默认签名，该字段填 ""
			86,             // 国家号
			sms.Mobiles[i], // 手机号
			433864,
			sms.Card,      // 短信正文ID
			sms.OrderTime, // 参数1
			sms.Name,
			sms.PhoneNumber,
			"",
		)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println(res)
		}
	}

}
