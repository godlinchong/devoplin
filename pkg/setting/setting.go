package setting

import (
	"log"
	"strings"
	"time"

	"github.com/go-ini/ini"
)

type App struct {
	JwtSecret       string
	PageSize        int
	RuntimeRootPath string

	ImagePrefixUrl string
	ImageSavePath  string
	ImageMaxSize   int
	ImageAllowExts []string

	LogSavePath string
	LogSaveName string
	LogFileExt  string
	TimeFormat  string
}

var AppSetting = &App{}

type Server struct {
	RunMode      string
	HttpPort     int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

var ServerSetting = &Server{}

type Database struct {
	Type        string
	User        string
	Password    string
	Host        string
	Name        string
	TablePrefix string
}

var DatabaseSetting = &Database{}

type Other struct {
	Mobiles string
	AppIDs  string
	AppKeys string
}
type Sdk struct {
	AppID  string
	AppKey string
}
type Others struct {
	Mobile []string
	App    []Sdk
}

var OthersSetting = &Others{
	Mobile: []string{},
	App:    []Sdk{},
}

func SetUp() {
	Cfg, err := ini.Load("conf/app.ini")
	if err != nil {
		log.Fatalf("Fail to parse 'conf/app.ini': %v", err)
	}

	err = Cfg.Section("app").MapTo(AppSetting)
	if err != nil {
		log.Fatalf("Cfg.MapTo AppSetting err: %v", err)
	}

	AppSetting.ImageMaxSize = AppSetting.ImageMaxSize * 1024 * 1024

	err = Cfg.Section("server").MapTo(ServerSetting)
	if err != nil {
		log.Fatalf("Cfg.MapTo ServerSetting err: %v", err)
	}

	ServerSetting.ReadTimeout = ServerSetting.ReadTimeout * time.Second
	ServerSetting.WriteTimeout = ServerSetting.ReadTimeout * time.Second

	err = Cfg.Section("database").MapTo(DatabaseSetting)
	if err != nil {
		log.Fatalf("Cfg.MapTo DatabaseSetting err: %v", err)
	}
	var OtherSetting = &Other{}
	err = Cfg.Section("other").MapTo(OtherSetting)
	mobiles := strings.Split(OtherSetting.Mobiles, ",")
	appIDs := strings.Split(OtherSetting.AppIDs, ",")
	appKeys := strings.Split(OtherSetting.AppKeys, ",")
	for i := 0; i < len(appIDs); i++ {
		OthersSetting.App = append(OthersSetting.App, Sdk{
			AppID:  appIDs[i],
			AppKey: appKeys[i],
		})
	}
	for i := 0; i < len(mobiles); i++ {
		OthersSetting.Mobile = append(OthersSetting.Mobile, mobiles[i])
	}
	log.Println(OthersSetting)
	if err != nil {
		log.Fatalf("Cfg.MapTo otherSetting err: %v", err)
	}

}
