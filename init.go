package main

import (
	"devoplin/models"
	"devoplin/pkg/logging"
	"devoplin/pkg/setting"
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

func init() {
	setting.SetUp()
	models.Setup()
	logging.SetUp()
}

const first1 int = 1288
const first2 int = 2388
const first3 int = 3588
const first4 int = 4688
const first5 int = 5888
const year int = 19

//1288（卡的型号）+19（年）+0001（四位）+12（随机两位）
func main() {
	makeUp(first1, year, 5000)
	makeUp(first2, year, 5000)
	makeUp(first3, year, 3000)
	makeUp(first4, year, 2000)
	makeUp(first5, year, 1000)

}

// 生成卡号
func makeUp(first int, year int, end int) {
	w := len(strconv.Itoa(end))
	end1 := pow(10, w)
	i := end1 + 1
	for {
		if i > end1+end {
			break
		}
		card := fmt.Sprintf("%d%d%s%s", first, year, strconv.Itoa(i)[1:], randLast(time.Now().UnixNano()))
		password := passwd(time.Now().UnixNano())
		fmt.Printf("%s:%s\n", card, password)
		//models.AddDzx(card,password)
		i++
	}

}

func randLast(seed int64) string {
	//step1: 设置种子数
	time.Sleep(10)
	rand.Seed(seed)
	//step2：获取随机数
	return fmt.Sprintf("%02v", rand.Intn(100))
}

func pow(x int, n int) int {
	if x == 0 {
		return 0
	}
	result := calPow(x, n)
	if n < 0 {
		result = 1 / result
	}
	return result
}

func calPow(x int, n int) int {
	if n == 0 {
		return 1
	}
	if n == 1 {
		return x
	}

	// 向右移动一位
	result := calPow(x, n>>1)
	result *= result

	// 如果n是奇数
	if n&1 == 1 {
		result *= x
	}

	return result
}

func passwd(seed int64) string {
	time.Sleep(10)
	rand.Seed(seed)
	return fmt.Sprintf("%09v", rand.Int31n(1000000000))
}
