package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//// 创建Sender
	//sender := &gosms.QSender{
	//	AppID:  "1400265529",                       // appid
	//	AppKey: "a2c6c16d0ae33224e39c81e634f28023", // appkey
	//}
	//
	//// 发送短信
	//res, err := sender.SingleSend(
	//	"楚食官",     // 短信签名，此处应填写审核通过的签名内容，非签名 ID，如果使用默认签名，该字段填 ""
	//	86,            // 国家号
	//	"13021023711", // 手机号
	//	433806,         // 短信正文ID
	//	"123456",      // 参数1
	//	"2",
	//	"3",
	//	"4",
	//)
	//if err != nil {
	//	fmt.Println(err)
	//} else {
	//	fmt.Println(res)
	//}
	for {
		fmt.Println(randKey(time.Now().UnixNano()))
	}
}

func randKey(seed int64) string {
	//step1: 设置种子数
	time.Sleep(10)
	rand.Seed(seed)
	//step2：获取随机数
	return fmt.Sprintf("%02v")
}
